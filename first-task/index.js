const addTwoNumbers = function (l1, l2) {
    const list1 = [];
    const list2 = [];
    const outputListNode = [];

    while (l1) {
        list1.push(l1.val);
        l1 = l1.next;
    }

    while (l2) {
        list2.push(l2.val);
        l2 = l2.next;
    }

    const firstNumber = +(list1.reverse().join(''));
    const secondNumber = +(list2.reverse().join(''));

    const reverseSum = String(firstNumber + secondNumber);
    const sum = reverseSum.split('').reverse();

    for (let i = 0; i < sum.length; i++) {
        outputListNode.push({
            val: sum[i],
            next: null,
        });
    }

    for (let i = 0; i < sum.length; i++) {
        outputListNode[i].next = outputListNode[i + 1] || null;
    }

    return outputListNode[0];
};