class LocalStorage {
    static setValue(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    static getValue(key) {
        const value = localStorage.getItem(key);

        return JSON.parse(value);
    }

    static deleteValue(key) {
        localStorage.removeItem(key);
    }
}