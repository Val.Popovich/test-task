class TasksStorage {
    static REMOVED_TASKS_STORAGE_KEY = 'removed_tasks';
    static ACTIVE_TASKS_STORAGE_KEY = 'active_tasks';
    activeTasks = [];
    removedTasks = [];

    constructor() {
        if (!TasksStorage.storage) {
            TasksStorage.storage = this;

            this.activeTasks = LocalStorage.getValue(TasksStorage.ACTIVE_TASKS_STORAGE_KEY);
            this.removedTasks = LocalStorage.getValue(TasksStorage.REMOVED_TASKS_STORAGE_KEY);
        }

        return TasksStorage.storage;
    }

    static buildTask(name, id) {
        return {
            id,
            name,
            marked: false,
        }
    }

    removeFromActiveTasksById(id) {
        this.activeTasks = this.activeTasks.filter((task) => id !== task.id);

        LocalStorage.setValue(TasksStorage.ACTIVE_TASKS_STORAGE_KEY, this.activeTasks);
    }

    removeFromRemovedTasksById(id) {
        this.removedTasks = this.removedTasks.filter((task) => id !== task.id);

        LocalStorage.setValue(TasksStorage.REMOVED_TASKS_STORAGE_KEY, this.removedTasks);
    }

    addToActiveTasks(name, id) {
        const task = TasksStorage.buildTask(name, id);
        this.activeTasks.push(task);

        LocalStorage.setValue(TasksStorage.ACTIVE_TASKS_STORAGE_KEY, this.activeTasks);
    }

    addToRemovedTasks(task) {
        this.removedTasks.push(task);

        LocalStorage.setValue(TasksStorage.REMOVED_TASKS_STORAGE_KEY, this.removedTasks);
    }

    getRemovedTasks() {
        return LocalStorage.getValue(TasksStorage.REMOVED_TASKS_STORAGE_KEY) || [];
    }

    getActiveTasks() {
        return LocalStorage.getValue(TasksStorage.ACTIVE_TASKS_STORAGE_KEY) || [];
    }

    getStrikeThroughTasks() {
        return this.activeTasks.filter(task => task.marked);
    }


    markTaskById(id) {
        this.activeTasks = this.activeTasks.map(task => {
            if (task.id === id) {
                task.marked = true;
            }

            return task;
        });
    }

    removeMarkedTasks() {
        this.removedTasks = this.getStrikeThroughTasks();
        LocalStorage.setValue(TasksStorage.REMOVED_TASKS_STORAGE_KEY, this.removedTasks);
        console.log('removedTasks' , this.removedTasks)

        this.activeTasks = this.activeTasks.filter(task => !task.marked);
        LocalStorage.setValue(TasksStorage.ACTIVE_TASKS_STORAGE_KEY, this.activeTasks);
    }

    toggleTaskMarkerById(id) {
        this.activeTasks = this.activeTasks.map(task => {
            if (task.id === id) {
                task.marked = !task.marked;
            }

            return task;
        });
    }
}