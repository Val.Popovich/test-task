const addTaskBtn = document.querySelector('.js-add-task');
const tasksWrapper = document.querySelector('.js-tasks');
const taskRemoveBtn = document.querySelector('.js-remove-tasks');
const cartElem = document.querySelector('.js-cart');
const toCartBtn = document.querySelector('.js-cart-btn');

const taskStorage = new TasksStorage();

renderTasks(taskStorage.activeTasks, tasksWrapper);

function renderTasks(tasks, parentEl) {
    clearTaskList();

    tasks.forEach(task => {
        renderTask(task.name, task.id, parentEl);
    });
}

addTaskBtn.addEventListener('click', (ev) => {
    ev.preventDefault();

    addTask();
});

taskRemoveBtn.addEventListener('click', (ev) => {
    ev.preventDefault();

    taskStorage.removeMarkedTasks();

    renderTasks(taskStorage.activeTasks, tasksWrapper);
});

toCartBtn.addEventListener('click', (ev) => {
    renderRemovedTasks();
});

function renderRemovedTasks() {
    clearTaskList();
    renderTasks(taskStorage.removedTasks, cartElem);
}

function addTask() {
    const taskId = Math.random().toString(16).slice(2);
    const taskName = document.querySelector('.text').value;

    taskStorage.addToActiveTasks(taskName, taskId);
    renderTask(taskName, taskId, tasksWrapper);
}

function renderTask(taskName, taskId, parentEl) {
    const taskElemWr = document.createElement('div');
    taskElemWr.classList.add('js-task-elem-wr');

    const strikeThroughBtn = document.createElement('input');
    strikeThroughBtn.classList.add('js-strike-through-btn');
    strikeThroughBtn.type = 'checkbox';
    strikeThroughBtn.id = taskId;
    taskElemWr.append(strikeThroughBtn);

    strikeThroughBtn.addEventListener('input', (ev) => {
        const boundedTaskElem = document.getElementById(`task-${taskId}`);
        boundedTaskElem.classList.toggle('_marked');

        taskStorage.toggleTaskMarkerById(taskId);
    });

    let taskElem = document.createElement('li');
    taskElem.textContent = taskName;
    taskElem.id = `task-${taskId}`;
    taskElemWr.append(taskElem);

    parentEl.append(taskElemWr);
}

function clearTaskList() {
    tasksWrapper.innerHTML = ' ';
}
